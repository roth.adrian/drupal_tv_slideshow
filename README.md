# Drupal TV slideshow

Show your articles of a drupal 8 webpage as a slideshow on a TV or similar screen. Originally created for the Division of Combustion Physics, Department of Physics, Lund University.

## Usage
There are (at least) two options of how to use this repository:
1. You buy a small computer that you can put next to your TV (such as a raspberry pi or rock) and use the TV as a screen for that computer. Then the computer will both download article from your drupal page and show the slideshow.
2. Your TV can connect to internet and show a html5 webpage with javascript. Then you need a separate server with this repository installed and an url/adress to the webpage that the server creates. This url is what the TV will use to show the webpage.

We have used option 1 so know most about that.

Create and edit a `config.json` file to tailor the TV slideshow for your needs.

Example:
```json
{
    "url": "https://combustionphysics.lu.se/",
    "title": "Combustion Physics",
    "subtitle": "Applied research in photonics and physics",
    "n_news": 10,
    "with_qrcode": false,
    "n_news_oldest": 0,
    "n_news_random": 0,
    "slideshow_delay": 30,
}
```
These are the current available options.

## Server Installation
Server is only tested on ubuntu and debian operating systems

- install python and makefiles (have only tested on python 3.8)
- run `make install` to install the required python packages in a virtual environment
- run `make crontab_updated` edit crontab to download articles from your drupal page one time each day
- install nginx or similar and create a site with the root directory pointed to this folder. See `tv.localhost` for an example site file for nginx.
- goto `tv.localhost` in your browser to see the results

## How it works
This code works in two parts.

The first part is the python script `article_crawler.py`. This script downloads all the articles of your drupal webpage and fills up an `index.html` with these articles. Here, the `index_base.html` is used as base for the output. The python script reads the `config.json` file to know how many of the latest articles to retrieve. This includes how many of the oldest articles and also how many random articles to retrieve.

When this index file is viewed through a server (such as nginx) the javascript and css in the `assets` folder will show a slideshow of all the articles that were downloaded. This is an infinite loop where you decide in the `config.json` file how many seconds each article will be shown. Some work has also been put into autoplaying videos (both internal and external (youtube and vimeo)) when the article is shown (super cool!!!)

If you need any help, please create an issue.

Good luck!

/Adrian
