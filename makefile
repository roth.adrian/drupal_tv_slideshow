.PHONY: install 

install: venv_drupal_tv_slideshow
	./venv_drupal_tv_slideshow/bin/python article_crawler.py
	# You also need to install nginx and point a site to this folder
	# then go to the site to see the webpage

venv_drupal_tv_slideshow: python_requirements.txt
	python3 -m venv $@
	touch $@
	./venv_drupal_tv_slideshow/bin/pip install -r python_requirements.txt

crontab_updated:
	# if you want automated updates of the tv
	crontab -l > $@
	# Update the articles once a day at 3am
	echo "0 3  * * * cd $$(pwd); git pull >> git.log; make >> make.log" >> $@
	crontab $@
