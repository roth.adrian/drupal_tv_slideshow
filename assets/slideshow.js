let progress_bar = document.getElementById("progress_bar");
let progress_running = false;
let time_step = 20;  // ms

let slides = document.getElementsByClassName("slide");
let dots = document.getElementsByClassName("dot");
let players = [];

var time_passed;
var frame_id;

function frame() {
  if (time_passed >= slideshow_delay) {
    clearInterval(frame_id);
    progress_running = false;
    next_slide();
  } else {
    time_passed += time_step;
    progress_bar.style.width = time_passed / slideshow_delay * 100 + "%";
  }
}

function progress() {
  if (!progress_running) {
    progress_running = true;
    time_passed = 0;
    frame_id = setInterval(frame, time_step);
  }
}

function next_slide() {
  var i;
  current_slide++;
  if (current_slide == slides.length * n_slideshow_revolutions) {
    location.reload();
  }
  if (current_slide > 0) {
    var current_last_slide = (current_slide - 1) % slides.length;
    slides[current_last_slide].style.display = "none";
    dots[current_last_slide].className = dots[current_last_slide].className.replace(" active", "");
    if (players[current_last_slide] !== null) {
      if (typeof(players[current_last_slide].stopVideo) === "function") {
        // youtube shit
        players[current_last_slide].stopVideo();
      } else if (typeof(players[current_last_slide].pause) === "function") {
        players[current_last_slide].pause();
      } else {
        console.log("Warning, the player was not paused")
      }
    }
  }
  var current_slide_index = current_slide % slides.length;
  slides[current_slide_index].style.display = "block";
  dots[current_slide_index].className += " active";
  if (players[current_slide_index] !== null) {
    if (typeof(players[current_slide_index].playVideo) === "function") {
      // youtube shit
      players[current_slide_index].playVideo();
    } else if (typeof(players[current_slide_index].play) === "function") {
      players[current_slide_index].play();
    } else {
      console.log("Warning, the player was not played")
    }
  }
  progress();
}

function show_slideshow() {
  var i;
  halfwidth = window.outerWidth * 0.5;
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  var loaded_players = 0, players_to_load = 0, ready_to_slide = false;

  onload = function(event) {
    loaded_players += 1;
    if (ready_to_slide && loaded_players == players_to_load) {
      // without timeout a starting youtube video might not be ready to play
      // so onReady should instead be onAlmostReady
      setTimeout(next_slide, 350);
    }
  };
  for (i = 0; i < slides.length; i++) {
    let videos = slides[i].getElementsByTagName("video");
    let iframes = slides[i].getElementsByTagName("iframe");
    if (videos.length > 0) {
      // internal videos
      players.push(videos[0]);
    } else if (iframes.length > 0) {
      // external videos
      iframe = iframes[0];
      iframe.height = halfwidth / parseFloat(iframe.attributes.aspect_ratio.value) + "px";

      try {
        if (iframe["src"].includes("youtube")) {
          iframe.id = "iframe" + i;
          var player = new YT.Player(iframe.id, { "onReady": onload });
          players.push(player);
          players_to_load += 1;
          // players.push(null);
        } else if (iframe["src"].includes("vimeo")) {
          var player = new Vimeo.Player(iframe);
          player.setLoop(true);
          players.push(player);
        } else {
          players.push(null);
        }
      } catch {
        players.push(null);
      }

    } else {
      players.push(null);
    }
  }

  ready_to_slide = true;
  if (players_to_load == 0) {
    next_slide();
  }
}

let current_slide = -1;
// show_slideshow();

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubeIframeAPIReady() {
  try {
    show_slideshow();
  } catch {
    // safety reload
    location.reload();
  }
}
