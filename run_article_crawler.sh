#!/usr/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. ${SCRIPT_DIR}/venv_drupal_tv_slideshow/bin/activate
python ${SCRIPT_DIR}/article_crawler.py
