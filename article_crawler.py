import numpy as np
import urllib.request
import difflib
import copy
import bs4
import json
import logging
import time

import qrcode
from pathlib import Path
import sys

file_dir = Path(__file__).parent
log_file = file_dir / "crawler.log"
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s;%(levelname)s;%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    handlers=[logging.FileHandler("debug.log"), logging.StreamHandler(sys.stdout)],
)


config_file = file_dir / "config.json"
base_config = {
    "url": "https://combustionphysics.lu.se/",
    "headers": {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0"  # noqa: E501
    },
    "n_news": 5,
    "with_qrcode": True,
    "n_news_oldest": 1,
    "n_news_random": 4,
    # "slideshow_delay": 30,
    # "n_slideshow_revolutions": 4,
}


article_dir = file_dir / "crawled_articles"
article_list_file = file_dir / "crawled_articles_list.json"


def find_full_size_article_img(article_link, thumb_img_src, config):
    try:
        headers = {}
        if "headers" in config:
            headers.update(config["headers"])
        req = urllib.request.Request(article_link, headers=headers)
        with urllib.request.urlopen(req) as f:
            html = f.read().decode("utf-8")
    except ConnectionError as e:
        # Might be bot protection from the webpage
        logging.warning(f"{e}")
        full_img_src = thumb_img_src.replace(
            "lu_list_thumbnail", "lu_article_medium_desktop_and_tablet"
        )
        return full_img_src
    html = html.replace("\\n", "")
    soup = bs4.BeautifulSoup(html, "html.parser")

    all_imgs = soup.find_all("img")
    best_score = 0
    best_img_src = None
    for img in all_imgs:
        score = difflib.SequenceMatcher(None, thumb_img_src, img["src"]).ratio()
        if score > best_score:
            best_img_src = img["src"]
            best_score = score
    if best_img_src is None:
        raise ValueError("Unexpected error in finding full article image path")
    full_img_src = best_img_src
    return full_img_src


def crawl():
    logging.info("Running article crawler")
    with open(file_dir / "index_base.html", "r") as f:
        base_html = f.read()
    output_soup = bs4.BeautifulSoup(base_html, "html.parser")

    config = base_config.copy()
    if config_file.is_file():
        with open(config_file, "r") as f:
            config.update(json.load(f))

    if "title" in config:
        output_soup.find(class_="header_title").string = config["title"]
    if "subtitle" in config:
        output_soup.find(class_="header_subtitle").string = config["subtitle"]

    base_url = config["url"]
    if base_url[-1] == "/":
        base_url = base_url[:-1]
        url = base_url
    if not url.endswith("/news/all"):
        url = f"{url}/news/all"
    else:
        base_url = url[: -len("/news/all")]

    headers = {}
    if "headers" in config:
        headers.update(config["headers"])
    all_li_tags = []

    # looping through pages of articles
    page_number = 0
    while True:
        current_url = f"{url}?page={page_number}"
        page_number += 1
        req = urllib.request.Request(current_url, headers=headers)
        for i in range(3):
            try:
                with urllib.request.urlopen(req) as f:
                    html = f.read().decode("utf-8")
                break
            except ConnectionError as e:
                logging.warning(f"Loading base page error, {e}")
                time.sleep(5)

        html = html.replace("\\n", "")
        soup = bs4.BeautifulSoup(html, "html.parser")

        list_uls = soup.find_all("ul")

        # trying to find the list on the page that best fits our expectation
        # take the last one if there are any problems
        best_fit_ind = -1
        best_fit_score = 0
        for i, list_ul in enumerate(list_uls):
            fit_score = 0
            current_li_tags = list_ul.find_all("li")
            for li_tag in current_li_tags:
                if len(li_tag.find_all("p")) == 2:
                    fit_score += 1 / 3
                if len(li_tag.find_all("h2")) == 1:
                    fit_score += 1 / 3
                if (
                    len(li_tag.find_all("img")) == 1
                    or len(li_tag.find_all("video")) == 1
                ):
                    fit_score += 1 / 3
            if fit_score > best_fit_score:
                best_fit_ind = i
                best_fit_score = fit_score
        if best_fit_score < 1:
            break
        list_ul = list_uls[best_fit_ind]
        all_li_tags.extend(list_ul.find_all("li"))
    if len(all_li_tags) == 0:
        raise ValueError("No articles found")

    # extracting the news defined in the config
    # all_li_tags = list_ul.find_all("li")
    li_tags = []
    if "n_news" in config and config["n_news"] > 0:
        li_tags.extend(all_li_tags[: config["n_news"]])
        all_li_tags = all_li_tags[config["n_news"] :]
    oldest_li_tags = None
    if "n_news_oldest" in config and config["n_news_oldest"] > 0:
        oldest_li_tags = all_li_tags[-config["n_news_oldest"] :]
        all_li_tags = all_li_tags[: -config["n_news_oldest"]]
    if (
        "n_news_random" in config
        and config["n_news_random"] > 0
        and len(all_li_tags) > 0
    ):
        randperm = np.random.permutation(len(all_li_tags))
        random_li_tags = [all_li_tags[i] for i in randperm[: config["n_news_random"]]]
        li_tags.extend(random_li_tags)
    # ensuring that the oldest articles are shown last
    if oldest_li_tags is not None:
        li_tags.extend(oldest_li_tags)

    single_dot = output_soup.new_tag("span")
    single_dot["class"] = "dot"
    slideshow_tag = output_soup.find(id="slideshow")
    dots_tag = output_soup.find(id="dots")
    for li in li_tags:
        path = Path(li.a["href"])
        name = path.name
        link = f"{base_url}{path}"
        logging.info(link)
        output_path = article_dir / name
        output_path.mkdir(exist_ok=True, parents=True)

        article_div = output_soup.new_tag("div")
        article_div["class"] = "article slide fade"
        article_div["style"] = "display: none;"

        article_img = li.find("img")
        article_video = li.find("video")
        article_external_video = li.find("iframe")
        if article_img is not None:
            article_img["src"] = find_full_size_article_img(
                link, article_img["src"], config
            )
            article_img["src"] = "{}{}".format(base_url, article_img["src"])
            article_media = article_img
            article_media["class"] = "article_media"
        elif article_video is not None:
            article_video.source["src"] = "{}{}".format(
                base_url, article_video.source["src"]
            )
            # play/pause is set in javascript
            # video["autoplay"] = "autoplay"
            article_video["muted"] = "muted"
            article_video["loop"] = "loop"
            if "controls" in article_video.attrs:
                del article_video.attrs["controls"]
            if "height" in article_video.attrs:
                del article_video.attrs["height"]
            if "width" in article_video.attrs:
                del article_video.attrs["width"]
            article_video["class"] = "video article_media"
            article_media = article_video
        elif article_external_video is not None:
            oembed_url = article_external_video["src"]
            oembed_url = oembed_url[oembed_url.find("url=") :]
            valid_source = True
            if "youtube" in article_external_video["src"]:
                oembed_url = f"https://www.youtube.com/oembed?{oembed_url}"
            elif "vimeo" in article_external_video["src"]:
                oembed_url = f"https://vimeo.com/api/oembed.json?{oembed_url}&controls=0&muted=1&loop=1"  # noqa: E501
            else:
                logging.warning(
                    "external video provider not supported {}".format(
                        article_external_video["src"]
                    )
                )
                valid_source = False

            if valid_source:
                with urllib.request.urlopen(oembed_url) as f:
                    oembed_information = json.loads(f.read().decode("utf-8"))

                iframe = bs4.BeautifulSoup(
                    oembed_information["html"], "html.parser"
                ).iframe
                aspect_ratio = (
                    oembed_information["width"] / oembed_information["height"]
                )
                iframe["aspect_ratio"] = aspect_ratio
                if "youtube" in iframe["src"]:
                    # hope to remove autoplay in the future
                    iframe["src"] = "{}&enablejsapi=1&controls=0&loop=1&mute=1".format(
                        iframe["src"]
                    )
                elif "vimeo" in iframe["src"]:
                    pass

                iframe["class"] = "video article_media"
                article_media = iframe
            else:
                article_media = output_soup.new_tag("div")
                article_media["class"] = "article_media"
        else:
            # no media found
            article_media = output_soup.new_tag("div")
            article_media["class"] = "article_media"
        article_div.append(article_media)

        # text
        article_text = output_soup.new_tag("div")
        article_text["class"] = "article_text"
        article_paragraphs = li.find_all("p")

        article_date = article_paragraphs[0]
        article_date["class"] = "article_date"
        article_text.append(article_date)

        article_title = li.find("h2")
        article_title["class"] = "article_title"
        article_text.append(article_title)

        article_lead = article_paragraphs[1]
        article_lead["class"] = "article_lead"
        article_text.append(article_lead)

        if config["with_qrcode"]:
            know_more_tag = output_soup.new_tag("div")
            know_more_tag["class"] = "know_more_tag"
            know_more_tag_text = output_soup.new_tag("p")
            know_more_tag_text["class"] = "know_more_tag_text"
            know_more_tag_text.string = "Want to know more?"
            know_more_tag.append(know_more_tag_text)

            img = qrcode.make(link)
            img_path = output_path / "qrcode.png"
            img.save(str(img_path))
            rel_img_path = img_path.relative_to(file_dir)
            qrcode_img = output_soup.new_tag("img", src=f"/{rel_img_path}")
            qrcode_img["class"] = "know_more_tag_img"
            know_more_tag.append(qrcode_img)
            article_text.append(know_more_tag)

        article_div.append(article_text)

        slideshow_tag.append(article_div)
        dots_tag.append(copy.copy(single_dot))

    if "slideshow_delay" in config:
        slideshow_delay = int(round(config["slideshow_delay"]))
    else:
        slideshow_delay = 30  # seconds

    if "n_slideshow_revolutions" in config:
        n_slideshow_revolutions = int(round(config["n_slideshow_revolutions"]))
    else:
        n_slideshow_revolutions = 4  # seconds

    full_slideshow_time = slideshow_delay * len(li_tags) * (n_slideshow_revolutions + 1)

    script_tag = output_soup.new_tag("script", type="text/javascript")
    script_tag.string = f"""
    slideshow_delay = {slideshow_delay * 1000};
    n_slideshow_revolutions = {n_slideshow_revolutions};

    // safety reload
    setTimeout(function(){{location.reload();}}, {full_slideshow_time * 1000});
"""
    dots_tag.insert_after(script_tag)

    with open(file_dir / "index.html", "w") as f:
        f.write(str(output_soup))


if __name__ == "__main__":
    import traceback

    try:
        crawl()
    except Exception:
        logging.error(traceback.format_exc())
